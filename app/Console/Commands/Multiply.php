<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Multiply extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Multiply {number1} {number2} {number3}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Multiply All given Number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number1 = $this->argument('number1');
        $number2 = $this->argument('number2');
        $number3 = $this->argument('number3');
        $result  = $this->multiply( $number1,$number2,$number3 );
        $this->info($number1." x ".$number2." x ".$number3." = ".$result);
    }

    protected function multiply($number1=0, $number2=0, $number3=0){
        $result=$number1 * $number2 * $number3;
        return $result;
    }
}
