<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Pow extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Pow {base} {exp}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exponent the given number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $base = $this->argument('base');
        $exp = $this->argument('exp');
        $result  = $this->pow( $base,$exp );
        $this->info($base." ^ ".$exp." = ".$result);
    }

    protected function pow($base=0, $exp=0){
        $result=pow($base , $exp);
        return $result;
    }
}
