<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Substrack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Substrack {number1} {number2} {number3}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Substrack All given Number';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $number1 = $this->argument('number1');
        $number2 = $this->argument('number2');
        $number3 = $this->argument('number3');
        $result  = $this->substrack( $number1,$number2,$number3 );
        $this->info($number1." - ".$number2." - ".$number3." = ".$result);
    }
    protected function substrack($number1=0, $number2=0, $number3=0){
        $result=$number1 - $number2 - $number3;
        return $result;
    }
}
